package scheduler

import (
	"cube/job"
	"cube/node"
	"cube/utils"
	"fmt"
	"testing"
)

func TestSelectCandidateNodes(t *testing.T) {
	task := job.NewTask("selectCandidateTest", "redis", 4096, 5000000)

	node1Stats := *utils.GetStats(32760, 25600, 4194304, 2097152, 1766, 1, 1134, 15638, 926, 0, 19, 0, 0, 0)
	node1 := node.NewNode("node1:3333", "", "worker")
	node1.Stats = node1Stats

	node2Stats := *utils.GetStats(32760, 25600, 4194304, 2097152, 14696006, 1552, 2834048, 827414099, 72770, 0, 19876, 0, 0, 0)
	node2 := node.NewNode("node2:3333", "", "worker")
	node2.Stats = node2Stats

	node3Stats := *utils.GetStats(32760, 25600, 4194304, 2097152, 14696006, 1552, 2834048, 827414099, 72770, 0, 19876, 0, 0, 0)
	node3 := node.NewNode("node3:3333", "", "worker")
	node3.Stats = node3Stats

	node1.Memory = 4194304
	node1.MemoryAllocated = 2097152
	node1.Disk = 32760
	node1.DiskAllocated = 7160

	node2.Memory = 4194304
	node2.MemoryAllocated = 1048576
	node2.Disk = 32760
	node2.DiskAllocated = 7160

	node3.Memory = 4194304
	node3.MemoryAllocated = 1048576
	node3.Disk = 32760
	node3.DiskAllocated = 7160

	candidates := SelectCandidateNodes(*task, []*node.Node{node1, node2, node3})
	var names []string
	for c := range candidates {
		names = append(names, candidates[c].Name)
	}
	fmt.Printf("Candidate nodes: %v\n", names)

	if len(candidates) != 3 {
		t.Errorf("Expected 3 candidates. Got %v\n", len(candidates))
	}
}

func TestSelectCandidateNodesNoCandidates(t *testing.T) {
	task := job.NewTask("selectCandidateTest", "redis", 32768, 5000000)

	node1Stats := *utils.GetStats(32760, 25600, 4194304, 2097152, 1766, 1, 1134, 15638, 926, 0, 19, 0, 0, 0)
	node1 := node.NewNode("node1", "", "worker")
	node1.Stats = node1Stats

	node2Stats := *utils.GetStats(32760, 25600, 4194304, 2097152, 14696006, 1552, 2834048, 827414099, 72770, 0, 19876, 0, 0, 0)
	node2 := node.NewNode("node2", "", "worker")
	node2.Stats = node2Stats

	node3Stats := *utils.GetStats(32760, 25600, 4194304, 2097152, 14696006, 1552, 2834048, 827414099, 72770, 0, 19876, 0, 0, 0)
	node3 := node.NewNode("node3", "", "worker")
	node3.Stats = node3Stats

	node1.Memory = 4194304
	node1.MemoryAllocated = 2097152
	node1.Disk = 32760
	node1.DiskAllocated = 7160

	node2.Memory = 4194304
	node2.MemoryAllocated = 1048576
	node2.Disk = 32760
	node2.DiskAllocated = 7160

	node3.Memory = 4194304
	node3.MemoryAllocated = 1048576
	node3.Disk = 32760
	node3.DiskAllocated = 7160

	candidates := SelectCandidateNodes(*task, []*node.Node{node1, node2, node3})
	var names []string
	for c := range candidates {
		names = append(names, candidates[c].Name)
	}
	fmt.Printf("Candidate nodes: %v\n", names)

	if len(candidates) != 0 {
		t.Errorf("Expected 0 candidates. Got %v\n", len(candidates))
	}
}

func TestSelectCandidateNodesOneCandidate(t *testing.T) {
	task := job.NewTask("selectCandidateTest", "redis", 10240, 5000000)

	node1Stats := *utils.GetStats(51200, 5120, 4194304, 2097152, 1766, 1, 1134, 15638, 926, 0, 19, 0, 0, 0)
	node1 := node.NewNode("node1", "", "worker")
	node1.Stats = node1Stats

	node2Stats := *utils.GetStats(51200, 25600, 4194304, 3145728, 14696006, 1552, 2834048, 827414099, 72770, 0, 19876, 0, 0, 0)
	node2 := node.NewNode("node2", "", "worker")
	node2.Stats = node2Stats

	node3Stats := *utils.GetStats(51200, 4096, 4194304, 3145728, 14696006, 1552, 2834048, 827414099, 72770, 0, 19876, 0, 0, 0)
	node3 := node.NewNode("node3", "", "worker")
	node3.Stats = node3Stats

	node1.Memory = 4194304
	node1.MemoryAllocated = 2097152
	node1.Disk = 51200
	node1.DiskAllocated = 46080

	node2.Memory = 4194304
	node2.MemoryAllocated = 1048576
	node2.Disk = 51200
	node2.DiskAllocated = 25600

	node3.Memory = 4194304
	node3.MemoryAllocated = 1048576
	node3.Disk = 51200
	node3.DiskAllocated = 47104

	candidates := SelectCandidateNodes(*task, []*node.Node{node1, node2, node3})
	var names []string
	for c := range candidates {
		names = append(names, candidates[c].Name)
	}
	fmt.Printf("Candidate nodes: %v\n", names)

	if len(candidates) != 1 {
		t.Errorf("Expected 1 candidates. Got %v\n", len(candidates))
	}

	if candidates[0].Name != "node2" {
		t.Errorf("Expected candidate node2. Got %s\n", candidates[0].Name)
	}
}
