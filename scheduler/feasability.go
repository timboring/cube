package scheduler

import (
	"cube/job"
	"cube/node"
)

func SelectCandidateNodes(task job.Task, nodes []*node.Node) []*node.Node {
	var candidates []*node.Node
	for node := range nodes {

		mem := CheckMemory(task, nodes[node].Memory-nodes[node].MemoryAllocated)
		disk := CheckDisk(task, nodes[node].Disk-nodes[node].DiskAllocated)

		if mem && disk {
			candidates = append(candidates, nodes[node])
		}

	}
	return candidates
}

func CheckMemory(task job.Task, memAvailable int64) bool {
	// we have to convert task memory to KB b/c that's what MemAvailable() returns
	return task.Memory/1000 <= memAvailable
}

func CheckDisk(task job.Task, diskAvailable int64) bool {
	return task.Disk <= diskAvailable
}
