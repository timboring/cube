package scheduler

import (
	"cube/job"
	"cube/node"
	"cube/stats"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"time"
)

const (
	// LIEB square ice constant
	LIEB = 1.53960071783900203869
)

type Scheduler interface {
	SelectCandidateNodes(task job.Task, nodes []*node.Node) []*node.Node
	Score(task job.Task, nodes []*node.Node) map[string]float64
	Pick(candidates map[string]float64, nodes []*node.Node) *node.Node
}

// Greedy Scheduling Algorithm
//
// This algorithm selects the machine with the minimum current CPU load.
type Greedy struct {
	Name string
}

func (g *Greedy) SelectCandidateNodes(task job.Task, nodes []*node.Node) []*node.Node {
	return SelectCandidateNodes(task, nodes)
}

func (g *Greedy) calculateLoad(usage float64, capacity float64) float64 {
	return usage / capacity
}

// Score nodes for task placement
//
// CPU load here is defined as current CPU usage (user + system + iowait + steal) divided
// by total capacity. This function uses the definition of CPU load as defined in the
// E-PVM paper cited below in the Enhanced PVM scheduler: it defines "total capacity"
// for CPU as the "smallest power of 2 integer greater than the largest load seen.
func (g *Greedy) Score(task job.Task, nodes []*node.Node) map[string]float64 {
	nodeScores := make(map[string]float64)

	for _, node := range nodes {
		cpuUsage := calculateCpuUsage(node)
		cpuLoad := g.calculateLoad(float64(cpuUsage), math.Pow(2, 0.8))
		nodeScores[node.Name] = cpuLoad
	}
	return nodeScores
}

func (g *Greedy) Pick(candidates map[string]float64, nodes []*node.Node) *node.Node {
	minCpu := 0.00
	var bestNode *node.Node
	for idx, node := range nodes {
		if idx == 0 {
			minCpu = candidates[node.Name]
			bestNode = node
		}

		if candidates[node.Name] < minCpu {
			minCpu = candidates[node.Name]
			bestNode = node
		}
	}
	return bestNode
}

// Enhanced PVM (Parallel Virtual Machine) Algorithm
//
// Implementation of the E-PVM algorithm laid out in http://www.cnds.jhu.edu/pub/papers/mosix.pdf.
// The algorithm calculates the "marginal cost" of assigning a task to a machine. In the paper and
// in this implementation, the only resources considered for calculating a task's marginal cost are
// memory and cpu.
type Epvm struct {
	Name string
}

func (e *Epvm) calculateLoad(usage float64, capacity float64) float64 {
	return (usage / capacity)
}

func (e *Epvm) SelectCandidateNodes(task job.Task, nodes []*node.Node) []*node.Node {
	return SelectCandidateNodes(task, nodes)
}

func (e *Epvm) calculateCost(newResourceLoad float64, nodeTaskCount float64, maxJobs float64, currentResourceLoad float64) float64 {
	cost := math.Pow(LIEB, newResourceLoad) + math.Pow(LIEB, nodeTaskCount+1/maxJobs) - math.Pow(LIEB, currentResourceLoad) - math.Pow(LIEB, nodeTaskCount/maxJobs)
	return cost
}

func (e *Epvm) Score(task job.Task, nodes []*node.Node) map[string]float64 {
	nodeScores := make(map[string]float64)
	maxJobs := 4.0
	maxCost := 1.0

	for _, node := range nodes {
		cpuUsage := calculateCpuUsage(node)
		cpuLoad := e.calculateLoad(cpuUsage, float64(math.Pow(2, 0.8)))

		memoryAllocated := float64(node.Stats.MemUsedKb()) + float64(node.MemoryAllocated)
		memoryPercentAllocated := memoryAllocated / float64(node.Memory)

		newMemPercent := (e.calculateLoad(memoryAllocated+float64(task.Memory/1000), float64(node.Memory)))
		memCost := math.Pow(LIEB, newMemPercent) + math.Pow(LIEB, (float64(node.TaskCount+1))/maxJobs) - math.Pow(LIEB, memoryPercentAllocated) - math.Pow(LIEB, float64(node.TaskCount)/float64(maxJobs))
		cpuCost := math.Pow(LIEB, cpuLoad) + math.Pow(LIEB, (float64(node.TaskCount+1))/maxJobs) - math.Pow(LIEB, cpuLoad) - math.Pow(LIEB, float64(node.TaskCount)/float64(maxJobs))

		marginalCost := memCost + cpuCost
		if marginalCost < maxCost {
			nodeScores[node.Name] = marginalCost
		}
	}
	return nodeScores
}

func (e *Epvm) Pick(candidates map[string]float64, nodes []*node.Node) *node.Node {
	minCost := 0.00
	var bestNode *node.Node
	for idx, node := range nodes {
		if idx == 0 {
			minCost = candidates[node.Name]
			bestNode = node
		}

		if candidates[node.Name] < minCost {
			minCost = candidates[node.Name]
			bestNode = node
		}
	}
	return bestNode
}

func getNodeStats(node *node.Node) *stats.Stats {
	url := fmt.Sprintf("%s/stats", node.Api)
	resp, err := http.Get(url)
	if err != nil {
		log.Printf("Error connecting to %v: %v", node.Api, err)
	}

	if resp.StatusCode != 200 {
		log.Printf("Error retrieving stats from %v: %v", node.Api, err)
	}

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	var stats stats.Stats
	json.Unmarshal(body, &stats)
	return &stats
}

// See discussion from this StackOverflow thread:
// https://stackoverflow.com/questions/23367857/accurate-calculation-of-cpu-usage-given-in-percentage-in-linux
func calculateCpuUsage(node *node.Node) float64 {
	// TODO: sleeping for 3 secs between calls to getNodeStats introduces a performance bottleneck. find alternatives?
	stat1 := getNodeStats(node)
	time.Sleep(3 * time.Second)
	stat2 := getNodeStats(node)

	stat1Idle := stat1.CpuStats.Idle + stat1.CpuStats.IOWait
	stat2Idle := stat2.CpuStats.Idle + stat2.CpuStats.IOWait

	stat1NonIdle := stat1.CpuStats.User + stat1.CpuStats.Nice + stat1.CpuStats.System + stat1.CpuStats.IRQ + stat1.CpuStats.SoftIRQ + stat1.CpuStats.Steal
	stat2NonIdle := stat2.CpuStats.User + stat2.CpuStats.Nice + stat2.CpuStats.System + stat2.CpuStats.IRQ + stat2.CpuStats.SoftIRQ + stat2.CpuStats.Steal

	stat1Total := stat1Idle + stat1NonIdle
	stat2Total := stat2Idle + stat2NonIdle

	total := stat2Total - stat1Total
	idle := stat2Idle - stat1Idle

	var cpuPercentUsage float64
	if total == 0 && idle == 0 {
		cpuPercentUsage = 0.00
	} else {
		cpuPercentUsage = (float64(total) - float64(idle)) / float64(total)
	}
	return cpuPercentUsage
}
