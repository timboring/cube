package cmd

import (
	"cube/manager"
	"fmt"
	"log"
	"time"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(managerCmd)
	managerCmd.Flags().StringSliceP("workers", "w", []string{"localhost:3333"}, "List of workers on which the manger will schedule jobs.")
	managerCmd.Flags().StringP("scheduler", "s", "epvm", "Name of scheduler to use.")
	managerCmd.Flags().StringP("task_db", "t", "tasks.db", "Filename to use for the tasks database")
	managerCmd.Flags().StringP("event_db", "e", "events.db", "Filename to use for the events database")
}

func processTasks(m *manager.Manager) {
	for {
		m.SendWork()
		log.Println("Sleeping for 10s.")
		time.Sleep(10 * time.Second)
	}
}

func updateTasks(m *manager.Manager) {
	for {
		log.Printf("Checking for task updates")
		m.UpdateTasks()
		log.Printf("Task update check completed.")
		log.Println("Sleeping for 10s.")
		time.Sleep(10 * time.Second)
	}
}

var managerCmd = &cobra.Command{
	Use:   "manager",
	Short: "Manager command to operate a Cube manager node.",
	Long: `cube manager command.

The manager is similar to a Borgmaster. It is responsible for managing the cluster:
- Accepting jobs from users
- Scheduler jobs onto worker nodes
- Rescheduler jobs in the event of a node failure
- Periodically polling workers to get updates of job and node state`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Starting manager.")
		workers, _ := cmd.Flags().GetStringSlice("workers")
		scheduler, _ := cmd.Flags().GetString("scheduler")
		taskDb, _ := cmd.Flags().GetString("task_db")
		eventDb, _ := cmd.Flags().GetString("event_db")

		m := manager.NewManager(workers, scheduler, taskDb, eventDb)
		api := manager.NewApi(&manager.ApiConfig{Address: "127.0.0.1", Port: 3334}, m)
		go processTasks(m)
		go updateTasks(m)
		go m.UpdateNodeStats()
		log.Printf("Creating new manager: %v", m)
		api.Start()
	},
}
