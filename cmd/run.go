package cmd

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(RunCmd)
	RunCmd.Flags().StringP("manager", "m", "localhost:3334", "Manager to talk to")
	RunCmd.Flags().StringP("file", "f", "task.json", "Task specification file")
}

var RunCmd = &cobra.Command{
	Use:   "run",
	Short: "Run a new job.",
	Long: `cube run command.

The run command runs a new job.`,
	Run: func(cmd *cobra.Command, args []string) {
		manager, _ := cmd.Flags().GetString("manager")
		// TODO add check to verify file exists and exit if not
		file, _ := cmd.Flags().GetString("file")

		log.Printf("Using manager: %v\n", manager)
		log.Printf("Using file: %v\n", file)

		data, err := ioutil.ReadFile(file)
		if err != nil {
			log.Fatalf("Unable to read file: %v", file)
		}
		log.Printf("Data: %v\n", string(data))

		url := fmt.Sprintf("http://%s/tasks", manager)
		resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
		if err != nil {
			log.Panic(err)
		}

		if resp.StatusCode != http.StatusOK {
			log.Printf("Error sending request: %v", resp.StatusCode)
		}

		defer resp.Body.Close()
		log.Printf("Sent task request to manager, %v", resp.StatusCode)
	},
}
