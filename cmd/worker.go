package cmd

import (
	"cube/worker"
	"fmt"
	"log"
	"time"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(workerCmd)
	workerCmd.Flags().StringP("host", "H", "0.0.0.0", "Hostname or IP address")
	workerCmd.Flags().IntP("port", "p", 3333, "Port on which to listen")
}

func runTasks(w *worker.Worker) {
	for {
		if w.Queue.Len() != 0 {
			result := w.RunTask()
			if result.Error != nil {
				log.Printf("Error running task: %v", result.Error)
			} else {
				// naively assuming success for now
				log.Print("Task completed successfully.")
			}
		} else {
			log.Printf("No tasks to process currently.")
		}
		log.Println("Sleeping for 10 seconds.")
		time.Sleep(10 * time.Second)
	}

}

var workerCmd = &cobra.Command{
	Use:   "worker",
	Short: "Worker command to operate a Cube worker node.",
	Long: `cube worker command.

The worker is similar to a Borglet. It performs the following functions:
- Runs jobs sent to it by the manager
- Attempt to restart failed jobs
- Responds to requests from the manager about its state`,
	Run: func(cmd *cobra.Command, args []string) {
		host, _ := cmd.Flags().GetString("host")
		port, _ := cmd.Flags().GetInt("port")
		fmt.Println("Starting worker.")
		w := worker.New()
		api := worker.NewApi(&worker.ApiConfig{Address: host, Port: port}, w)
		go runTasks(w)
		go w.CollectStats()
		go w.UpdateTaskCount()
		log.Printf("Create new worker: %v", w)
		api.Start()
	},
}
