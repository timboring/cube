package cmd

import (
	"cube/job"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"text/tabwriter"
	"time"

	"github.com/docker/go-units"
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(StatusCmd)
	StatusCmd.Flags().StringP("manager", "m", "localhost:3334", "Manager to talk to")
}

var StatusCmd = &cobra.Command{
	Use:   "status",
	Short: "Status command to list jobs.",
	Long: `cube status command.

The status command allows a user to get the status of jobs from the Cube manager.`,
	Args: cobra.MaximumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		manager, _ := cmd.Flags().GetString("manager")
		var resource string
		if len(args) > 0 {
			resource = args[0]
		}
		if resource == "events" {
			url := fmt.Sprintf("http://%s/events", manager)
			resp, _ := http.Get(url)
			defer resp.Body.Close()
			body, _ := ioutil.ReadAll(resp.Body)
			var events []job.TaskEvent
			json.Unmarshal(body, &events)
			w := tabwriter.NewWriter(os.Stdout, 0, 0, 5, ' ', tabwriter.TabIndent)
			fmt.Fprintf(w, "ID\tTask ID\tAction\tTimestamp\t\n")
			for _, event := range events {
				action := event.Action.String()[event.Action]
				fmt.Fprintf(w, "%s\t%s\t%s\t%s\t\n", event.ID, event.TaskID, action, event.Timestamp)
			}
			w.Flush()
		} else {
			url := fmt.Sprintf("http://%s/tasks", manager)
			resp, _ := http.Get(url)
			defer resp.Body.Close()
			body, _ := ioutil.ReadAll(resp.Body)
			var tasks []*job.Task
			json.Unmarshal(body, &tasks)
			w := tabwriter.NewWriter(os.Stdout, 0, 0, 5, ' ', tabwriter.TabIndent)
			fmt.Fprintln(w, "ID\tNAME\tCREATED\tSTATE\tCONTAINERNAME\tIMAGE\tNODE\t")
			for _, task := range tasks {
				var start string
				if task.StartTime.IsZero() {
					start = fmt.Sprintf("%s ago", units.HumanDuration(time.Now().UTC().Sub(time.Now().UTC())))
				} else {
					start = fmt.Sprintf("%s ago", units.HumanDuration(time.Now().UTC().Sub(task.StartTime)))
				}
				state := task.State.String()[task.State]
				fmt.Fprintf(w, "%s\t%s\t%s\t%s\t%s\t%s\t%s\t\n", task.ID, task.Name, start, state, task.Name, task.Image, task.Runtime.Node)
			}
			w.Flush()
		}
	},
}
