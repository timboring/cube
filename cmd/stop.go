package cmd

import (
	"fmt"
	"log"
	"net/http"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(stopCmd)
	stopCmd.Flags().StringP("manager", "m", "localhost:3334", "Manager to talk to")
}

var stopCmd = &cobra.Command{
	Use:   "stop",
	Short: "Stop a running job.",
	Long: `cube stop command.

cube stop stops a running job.`,
	Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		manager, _ := cmd.Flags().GetString("manager")
		url := fmt.Sprintf("http://%s/tasks/%s", manager, args[0])
		client := &http.Client{}
		req, err := http.NewRequest("DELETE", url, nil)
		if err != nil {
			log.Printf("Error creating request %v: %v", url, err)
		}

		resp, err := client.Do(req)
		if err != nil {
			log.Printf("Error connecting to %v: %v", url, err)
		}

		if resp.StatusCode != http.StatusOK {
			log.Printf("Error sending request: %v", err)
		}

		log.Printf("Task %v has been stopped.", args[0])
	},
}
