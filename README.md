# Cube: A Borg Clone for Fun and (Not) Profit

The purpose of this project is to learn more about how scheduling/orchestration systems
like Borg, Kubernetes, and Nomad work. And what better way to learn about these systems
than to implement a clone of them, more-or-less, from scratch.

The design for this project tries to follow the design laid out in the original Borg
[paper](https://storage.googleapis.com/pub-tools-public-publication-data/pdf/43438.pdf).
However, it also takes some inspiration from Hashicorp's [Nomad], which itself drew
[inspiration](https://www.nomadproject.io/docs/internals/scheduling) from Borg. That said,
the larger goal is not simply to produce a faithful copy of Borg (or Nomad), but to learn
about the design decisions that go into building these kinds of systems.

## About the Name
`Cube` is a reference to the transport used by the Borg from Star Trek.

## Building the project
Building the project results in a single binary, which can then be moved to an appropriate
place on your path from which it can be run.

```sh
> go build 
```

To build the binary for a different target architecture, set the `GOARCH` environment variable:
```sh
> GOARCH=arm go build
```

To build the binary for a different operating system, set the `GOOS` environment variable:
```sh
> GOOS=darwin go build
```

## Starting the Worker
```go
> ./cube worker
```
The above command will start a Cube worker running locally and listening on port 3333.

It provides an API, which can be queried using any typical REST client:
```sh
# using curl
> curl http://localhost:3333/tasks
{}

# using httpie
> http :3333/tasks
HTTP/1.1 200 OK
Content-Length: 3
Content-Type: application/json
Date: Tue, 22 Sep 2020 02:10:07 GMT

{}
```

## Starting the Manager
```go
./cube manager -w $worker1:3333,$worker2:3333
```
The above command will start a Cube manager running locally and listening on port 3334.

Like the worker, the manager also has an API:
```sh
# using curl
> curl http://localhost:3334/tasks
{}

# using httpie
http :3334/tasks
HTTP/1.1 200 OK
Content-Length: 3
Content-Type: application/json
Date: Tue, 22 Sep 2020 02:10:56 GMT

{}
```

## Using the CLI
```sh
./cube -h
cube is the main command to interact with a Cube cluster.
		
Cube is a simplistic clone of Google's Borg. It is intended
to be an exercise in learning how to write and operate a scheduler.

Usage:
  cube [command]

Available Commands:
  help        Help about any command
  manager     Manager command to operate a Cube manager node.
  run         Run a new job.
  status      Status command to list jobs.
  stop        Stop a running job.
  worker      Worker command to operate a Cube worker node.

Flags:
      --config string   config file (default is $HOME/.cobra.yaml)
  -h, --help            help for cube

Use "cube [command] --help" for more information about a command.
```
