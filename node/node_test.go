package node

import (
	"cube/utils"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestNodeInitialization(t *testing.T) {
	n := NewNode("test-node", "http://test-node:3333", "worker")
	if n.Name != "test-node" {
		t.Errorf("Expected %v; Got %v", "test-node", n.Name)
	}

	if n.Api != "http://test-node:3333" {
		t.Errorf("Expected %v; Got %v", "http://test-node:3333", n.Api)
	}

	if n.Role != "worker" {
		t.Errorf("Expected %v; Got %v", "worker", n.Role)
	}
}

func TestGetNodeInfo(t *testing.T) {
	nodeStats := utils.GetStats(32760, 25600, 10240, 5120, 2435287, 360, 469586, 139575590, 14554, 0, 3448, 0, 0, 0)

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		json.NewEncoder(w).Encode(nodeStats)
	}))

	n := NewNode("test-node", ts.URL, "worker")

	n.GetInfo()

	if n.Stats.MemTotalKb() != uint64(10240) {
		t.Errorf("Expected 10240; Got %v\n", n.Stats.MemTotalKb())
	}

	if n.Stats.DiskTotal() != uint64(32760) {
		t.Errorf("Expected 32760; Got %v\n", n.Stats.DiskTotal())
	}

	if len(n.Stats.CpuInfo.Processors) != 1 {
		t.Errorf("Expected 1 processor; Got %v\n", len(n.Stats.CpuInfo.Processors))
	}

	if n.Stats.CpuStats.User != 2435287 {
		t.Errorf("Expected 3; Got %v\n", n.Stats.CpuStats.User)
	}
}
