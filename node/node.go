package node

import (
	"cube/stats"
	"cube/utils"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

// Node representation of a Cube node
type Node struct {
	Name string
	Ip   string
	// Api e.g. "http://worker-1:3333"
	Api string
	// Cores number of cores
	Cores int
	// Memory amount of total memory in KiB
	Memory int64
	// MemoryAllocated amount of allocated to existing tasks in KiB
	MemoryAllocated int64
	// Disk size of disk in KiB
	Disk int64
	// DiskAllocated amount of disk allocated to existing tasks in KiB
	DiskAllocated int64
	// Usage current usage stats
	Stats stats.Stats
	// Role one of ["manager", "worker"]
	Role string
	// TaskCount number of tasks currently running on the node
	TaskCount int
}

func NewNode(name string, api string, role string) *Node {
	return &Node{Name: name, Api: api, Role: role}
}

func (n *Node) GetInfo() {
	var resp *http.Response
	var err error

	url := fmt.Sprintf("%s/stats", n.Api)
	resp, err = utils.HTTPWithRetry(http.Get, url)
	if err != nil {
		log.Printf("Unable to connect to %v. Permanent failure.\n", n.Api)
		return
	}

	if resp.StatusCode != 200 {
		log.Printf("Error retrieving stats from %v: %v", n.Api, err)
	}

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	var stats stats.Stats
	json.Unmarshal(body, &stats)

	n.Memory = int64(stats.MemTotalKb())
	n.Disk = int64(stats.DiskTotal())
	n.Cores = stats.CpuCores()

	n.Stats = stats
}

type NodeUsage struct {
	CpuUsage  float32
	MemUsage  float32
	DiskUsage float32
}
