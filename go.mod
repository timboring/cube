module cube

go 1.14

require (
	github.com/Microsoft/go-winio v0.4.14 // indirect
	github.com/boltdb/bolt v1.3.1
	github.com/c9s/goprocinfo v0.0.0-20200311234719-5750cbd54a3b
	github.com/containerd/containerd v1.4.0 // indirect
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/docker v17.12.0-ce-rc1.0.20200901185902-ae0ef82b9035+incompatible
	github.com/docker/go-connections v0.4.0
	github.com/docker/go-units v0.4.0
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/golang-collections/collections v0.0.0-20130729185459-604e922904d3
	github.com/google/uuid v1.1.2
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/moby/moby v1.13.1
	github.com/moby/term v0.0.0-20200915141129-7f0af18e79f2 // indirect
	github.com/morikuni/aec v1.0.0 // indirect
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/opencontainers/image-spec v1.0.1 // indirect
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.4.0 // indirect
	golang.org/x/net v0.0.0-20200513185701-a91f0712d120 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
