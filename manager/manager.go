package manager

import (
	"bytes"
	"cube/job"
	"cube/node"
	"cube/scheduler"
	"cube/store"
	"cube/utils"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/golang-collections/collections/queue"
	"github.com/google/uuid"
)

// Manager concrete type representing Manager object
type Manager struct {
	pending       queue.Queue
	taskDb        store.Store
	eventDb       store.Store
	workers       []string
	workerTaskMap map[string][]uuid.UUID
	taskWorkerMap map[uuid.UUID]string
	workerNodes   []*node.Node
	workerMap     map[string]*node.Node
	scheduler     scheduler.Scheduler
}

// NewManager constructor
func NewManager(workers []string, s string, taskStoreFileName string, eventStoreFileName string) *Manager {
	taskDb := store.NewStore(store.Task, taskStoreFileName, 0600, "tasks")
	eventDb := store.NewStore(store.Event, eventStoreFileName, 0600, "events")

	workerTaskMap := make(map[string][]uuid.UUID)
	taskWorkerMap := make(map[uuid.UUID]string)
	workerMap := make(map[string]*node.Node)
	var nodes []*node.Node
	for worker := range workers {
		workerTaskMap[workers[worker]] = []uuid.UUID{}

		nAPI := fmt.Sprintf("http://%v", workers[worker])
		n := node.NewNode(workers[worker], nAPI, "worker")
		// We call GetInfo() here because the worker node is the authority of its own state
		//n.GetInfo()
		nodes = append(nodes, n)
		workerMap[n.Name] = n

	}

	result := taskDb.List()
	tasks := result.Result.([]*job.Task)
	for _, task := range tasks {
		workerTaskMap[task.Runtime.Node] = append(workerTaskMap[task.Runtime.Node], task.ID)
		taskWorkerMap[task.ID] = task.Runtime.Node
	}

	var sch scheduler.Scheduler
	switch s {
	case "greedy":
		sch = &scheduler.Greedy{}
	default:
		sch = &scheduler.Epvm{}
	}

	return &Manager{
		pending:       *queue.New(),
		workers:       workers,
		taskDb:        taskDb,
		eventDb:       eventDb,
		workerTaskMap: workerTaskMap,
		taskWorkerMap: taskWorkerMap,
		workerNodes:   nodes,
		workerMap:     workerMap,
		scheduler:     sch,
	}
}

// UpdateNodeStats continuously updates worker node stats
func (m *Manager) UpdateNodeStats() {
	for {
		for _, node := range m.workerNodes {
			log.Printf("Collecting stats for node %v", node.Name)
			node.GetInfo()
		}
		time.Sleep(15 * time.Second)
	}
}

func (m *Manager) selectWorker(task job.Task) (*node.Node, error) {
	candidates := m.scheduler.SelectCandidateNodes(task, m.workerNodes)
	if candidates == nil {
		msg := fmt.Sprintf("No available candidates match resource request for task %v", task.ID)
		err := errors.New(msg)
		return nil, err
	}
	scores := m.scheduler.Score(task, candidates)
	selectedNode := m.scheduler.Pick(scores, candidates)

	return selectedNode, nil
}

// SendWork send a task to a Worker
func (m *Manager) SendWork() {

	// Send work to an instance of Worker
	if m.pending.Len() > 0 {
		e := m.pending.Dequeue()
		te := e.(job.TaskEvent)

		m.eventDb.Put(te.ID.String(), &te)
		task := te.Task
		worker, err := m.selectWorker(task)
		if err != nil {
			log.Printf("Error: %v", err)
			return
		}

		m.workerTaskMap[worker.Name] = append(m.workerTaskMap[worker.Name], task.ID)
		m.taskWorkerMap[task.ID] = worker.Name
		log.Printf("Pulled %v off pending queue", task.String())

		task.State = job.Scheduled
		task.Runtime.Node = worker.Name
		m.taskDb.Put(task.ID.String(), &task)

		body, err := json.Marshal(task)
		if err != nil {
			log.Printf("Unable to marshal task object: %v.", task)
		}

		url := fmt.Sprintf("http://%s/tasks", worker.Name)
		resp, err := http.Post(url, "application/json", bytes.NewBuffer(body))
		if err != nil {
			log.Printf("Error connecting to %v: %v", worker, err)
			m.pending.Enqueue(task)
			return
		}
		defer resp.Body.Close()

		if resp.StatusCode != 204 {
			log.Printf("Error sending request: %v", resp.StatusCode)
		}

		worker.TaskCount++
		worker.MemoryAllocated += task.Memory / 1000
		worker.DiskAllocated += task.Disk

		// TODO: Do we need to do anything with resp.Body here?
		_, _ = ioutil.ReadAll(resp.Body)
		log.Printf("[%v] Enqueued task %v with worker at %s", resp.StatusCode, task.ID, worker.Name)
	} else {
		log.Println("No work in the queue")
	}
}

// UpdateTasks retrieve task state from workers
func (m *Manager) UpdateTasks() {
	for worker := range m.workers {
		var resp *http.Response
		var err error
		log.Printf("Checking worker %v for task updates", m.workers[worker])

		url := fmt.Sprintf("http://%s/tasks", m.workers[worker])
		resp, err = utils.HTTPWithRetry(http.Get, url)
		if err != nil {
			log.Printf("Unable to connect to %s. Permanent failure.\n", url)
			return
		}

		if resp.StatusCode != http.StatusOK {
			log.Printf("Error sending request: %v", err)
		}

		body, err := ioutil.ReadAll(resp.Body)
		defer resp.Body.Close()

		if err != nil {
			log.Printf("Error reading request body: %s", err)
		}

		var tasks []*job.Task
		json.Unmarshal(body, &tasks)

		for _, task := range tasks {
			log.Printf("Attempting to update task %v", task.ID)
			queryResult := m.taskDb.Get(task.ID.String())
			if queryResult.Error != nil {
				log.Printf("Cannot find task %s. Skipping.\n", task.ID)
				continue
			}

			persistedTask := queryResult.Result.(*job.Task)
			if persistedTask.State != task.State {
				persistedTask.State = task.State
			}

			persistedTask.StartTime = task.StartTime
			persistedTask.FinishTime = task.FinishTime

			// For now, blinding copying the value of the ContainerID field
			persistedTask.Runtime.ContainerID = task.Runtime.ContainerID

			m.taskDb.Put(persistedTask.ID.String(), persistedTask)
		}

	}
}

func (m *Manager) addTask(t job.TaskEvent) {
	m.pending.Enqueue(t)
	m.eventDb.Put(t.ID.String(), &t)
}
