package manager

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
)

// ApiConfig configuration used for the API
type ApiConfig struct {
	Address string
	Port    int
}

type Api struct {
	cfg     *ApiConfig
	stopped bool
	manager *Manager
	router  *chi.Mux
}

func NewApi(cfg *ApiConfig, m *Manager) *Api {
	return &Api{
		cfg:     cfg,
		manager: m,
	}
}

func (a *Api) Start() {
	a.initRouter()
	http.ListenAndServe(fmt.Sprintf("%s:%d", a.cfg.Address, a.cfg.Port), a.router)
}

func (a *Api) Stop() error {
	return nil
}
