package manager

import (
	"cube/job"
	"cube/utils"
	"os"
	"testing"
)

func cleanup(testTaskDb string, testEventDb string) {
	os.Remove(testTaskDb)
	os.Remove(testEventDb)
}

func TestSelectWorkerSingleCandidateWithEpvmScheduler(t *testing.T) {
	testTaskDb := "test_tasks.db"
	testEventDb := "test_events.db"
	task := job.NewTask("test", "redis", 2048, 1048576000)

	node1Stats := *utils.GetStats(32760, 25600, 4194304, 307200, 1766, 1, 1134, 15638, 926, 0, 19, 0, 0, 0)
	node2Stats := *utils.GetStats(32760, 25600, 4194304, 2097152, 14696006, 1552, 2834048, 827414099, 72770, 0, 19876, 0, 0, 0)
	node3Stats := *utils.GetStats(32760, 25600, 4194304, 1048576, 14696006, 1552, 2834048, 827414099, 72770, 0, 19876, 0, 0, 0)

	ts1 := utils.CreateTestServer(node1Stats)
	ts2 := utils.CreateTestServer(node2Stats)
	ts3 := utils.CreateTestServer(node3Stats)

	manager := NewManager([]string{"node1:3333", "node2:3333", "node3:3333"}, "epvm", testTaskDb, testEventDb)
	manager.workerNodes[0].Api = ts1.URL
	manager.workerNodes[1].Api = ts2.URL
	manager.workerNodes[2].Api = ts3.URL

	for i := 0; i < 3; i++ {
		manager.workerNodes[i].GetInfo()
	}

	manager.workerNodes[0].MemoryAllocated = 2097152
	manager.workerNodes[0].DiskAllocated = 7160

	manager.workerNodes[1].MemoryAllocated = 1048576
	manager.workerNodes[1].DiskAllocated = 7160

	manager.workerNodes[2].MemoryAllocated = 1048576
	manager.workerNodes[2].DiskAllocated = 7160

	selectedNode, err := manager.selectWorker(*task)
	if err != nil {
		t.Errorf("selectWorker() returned unexpected err: %v", err)
	}
	if selectedNode.Name != "node3:3333" {
		t.Errorf("Expected node3:3333; Got %v", selectedNode.Name)
	}

	cleanup(testTaskDb, testEventDb)
}

func TestSelectWorkerMultipleCandidatesWithEpvmScheduler(t *testing.T) {
	testTaskDb := "test_tasks.db"
	testEventDb := "test_events.db"
	task := job.NewTask("test", "redis", 2048, 1048576000)

	node1Stats := *utils.GetStats(32760, 8192, 4194304, 2097152, 1766, 1, 1134, 15638, 926, 0, 19, 0, 0, 0)
	node2Stats := *utils.GetStats(32760, 4096, 4194304, 307200, 14696006, 1552, 2834048, 827414099, 72770, 0, 19876, 0, 0, 0)
	node3Stats := *utils.GetStats(32760, 25600, 4194304, 307200, 14696006, 1552, 2834048, 827414099, 72770, 0, 19876, 0, 0, 0)

	ts1 := utils.CreateTestServer(node1Stats)
	ts2 := utils.CreateTestServer(node2Stats)
	ts3 := utils.CreateTestServer(node3Stats)

	manager := NewManager([]string{"node1:3333", "node2:3333", "node3:3333"}, "epvm", testTaskDb, testEventDb)
	manager.workerNodes[0].Api = ts1.URL
	manager.workerNodes[1].Api = ts2.URL
	manager.workerNodes[2].Api = ts3.URL

	for i := 0; i < 3; i++ {
		manager.workerNodes[i].GetInfo()
	}

	manager.workerNodes[0].MemoryAllocated = 2097152
	manager.workerNodes[0].DiskAllocated = 7160

	manager.workerNodes[1].MemoryAllocated = 1048576
	manager.workerNodes[1].DiskAllocated = 7160

	manager.workerNodes[2].MemoryAllocated = 1048576
	manager.workerNodes[2].DiskAllocated = 7160

	selectedNode, err := manager.selectWorker(*task)
	if err != nil {
		t.Errorf("selectWorker() returned unexpected err: %v", err)
	}
	if selectedNode.Name != "node2:3333" {
		t.Errorf("Expected node2:3333; Got %v", selectedNode.Name)
	}

	cleanup(testTaskDb, testEventDb)
}

func TestTwoAllocationsSelectDifferentNodes(t *testing.T) {
	testTaskDb := "test_tasks.db"
	testEventDb := "test_events.db"
	task1 := job.NewTask("test1", "redis", 2048, 1048576000)
	task2 := job.NewTask("test2", "redis", 2048, 1048576000)

	node1Stats := *utils.GetStats(32760, 8192, 4194304, 2097152, 1766, 1, 1134, 15638, 926, 0, 19, 0, 0, 0)
	node2Stats := *utils.GetStats(32760, 4096, 4194304, 307200, 14696006, 1552, 2834048, 827414099, 72770, 0, 19876, 0, 0, 0)
	node3Stats := *utils.GetStats(32760, 25600, 4194304, 307200, 14696006, 1552, 2834048, 827414099, 72770, 0, 19876, 0, 0, 0)

	ts1 := utils.CreateTestServer(node1Stats)
	ts2 := utils.CreateTestServer(node2Stats)
	ts3 := utils.CreateTestServer(node3Stats)

	manager := NewManager([]string{"node1:3333", "node2:3333", "node3:3333"}, "epvm", testTaskDb, testEventDb)
	manager.workerNodes[0].Api = ts1.URL
	manager.workerNodes[1].Api = ts2.URL
	manager.workerNodes[2].Api = ts3.URL

	for i := 0; i < 3; i++ {
		manager.workerNodes[i].GetInfo()
	}

	manager.workerNodes[0].MemoryAllocated = 2097152
	manager.workerNodes[0].DiskAllocated = 7160

	manager.workerNodes[1].MemoryAllocated = 1048576
	manager.workerNodes[1].DiskAllocated = 7160

	manager.workerNodes[2].MemoryAllocated = 1048576
	manager.workerNodes[2].DiskAllocated = 7160

	selectedNode1, _ := manager.selectWorker(*task1)

	node1Stats = *utils.GetStats(32760, 8192, 4194304, 2097152, 1766, 1, 1134, 15638, 926, 0, 19, 0, 0, 0)
	node2Stats = *utils.GetStats(32760, 4096, 4194304, 512000, 14696006, 1552, 2834048, 827414099, 72770, 0, 19876, 0, 0, 0)
	node3Stats = *utils.GetStats(32760, 25600, 4194304, 307200, 14696006, 1552, 2834048, 827414099, 72770, 0, 19876, 0, 0, 0)

	for i := 0; i < 3; i++ {
		manager.workerNodes[i].GetInfo()
	}

	manager.workerNodes[1].MemoryAllocated = 2097152
	manager.workerNodes[1].DiskAllocated = 5160

	selectedNode2, _ := manager.selectWorker(*task2)

	if selectedNode1.Name == selectedNode2.Name {
		t.Errorf("Expected different nodes. Got: %v and %v", selectedNode1.Name, selectedNode2.Name)
	}

	cleanup(testTaskDb, testEventDb)
}
