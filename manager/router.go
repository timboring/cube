package manager

import (
	"cube/job"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/go-chi/chi"
	"github.com/google/uuid"
)

func (a *Api) initRouter() {
	a.router = chi.NewRouter()
	a.router.Route("/tasks", func(r chi.Router) {
		r.Post("/", a.ReceiveTaskHandler)
		r.Get("/", a.GetTasksHandler)
		r.Route("/{jobID}", func(r chi.Router) {
			r.Delete("/", a.StopTaskHandler)
		})
	})
	a.router.Route("/events", func(r chi.Router) {
		r.Get("/", a.GetTaskEventsHandler)
	})
	a.router.Route("/nodes", func(r chi.Router) {
		r.Get("/", a.GetNodesHandler)
	})
}

func (a *Api) ReceiveTaskHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		log.Printf("Error reading request body: %v", err)
	}

	t := job.Task{}
	t.Mu = &sync.Mutex{}
	err = json.Unmarshal(body, &t)
	if err != nil {
		log.Printf("error: %v", err)
	}

	t.ID, err = uuid.NewRandom()
	eventID, _ := uuid.NewRandom()
	t.State = job.Pending
	te := job.TaskEvent{
		ID:        eventID,
		TaskID:    t.ID,
		Action:    job.Schedule,
		Timestamp: time.Now().UTC(),
		Task:      t,
	}
	a.manager.addTask(te)
	log.Printf("Added task %v to the pending queue\n", t)
	w.WriteHeader(204)
}

func (a *Api) GetNodesHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(a.manager.workerNodes)
}

// GetTasksHandler handler for REST API
func (a *Api) GetTasksHandler(w http.ResponseWriter, r *http.Request) {
	queryResult := a.manager.taskDb.List()
	tasks := queryResult.Result.([]*job.Task)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(tasks)
}

// GetTaskEventsHandler handler for REST API
func (a *Api) GetTaskEventsHandler(w http.ResponseWriter, r *http.Request) {
	queryResult := a.manager.eventDb.List()
	events := queryResult.Result.([]*job.TaskEvent)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(events)
}

// StopTaskHandler handles stop tasks
func (a *Api) StopTaskHandler(w http.ResponseWriter, r *http.Request) {
	jobID := chi.URLParam(r, "jobID")
	log.Printf("Attempting to stop task %v", jobID)

	if jobID != "" {
		j, _ := uuid.Parse(jobID)
		queryResult := a.manager.taskDb.Get(j.String())
		if queryResult.Error != nil {
			log.Printf("Unable to find task %v", jobID)
			w.WriteHeader(404)
		}

		task := queryResult.Result.(*job.Task)
		log.Printf("Found job %v", task.String())

		client := &http.Client{}
		worker := a.manager.taskWorkerMap[j]
		url := fmt.Sprintf("http://%s/tasks/%s", worker, jobID)
		req, err := http.NewRequest("DELETE", url, nil)
		if err != nil {
			log.Printf("Error creating request %v: %v", worker, err)
		}

		resp, err := client.Do(req)
		if err != nil {
			log.Printf("Error connecting to %v: %v", worker, err)
		}

		if resp.StatusCode != 204 {
			log.Printf("Error sending request: %v", err)
		}

		log.Printf("Task %v has been scheduled to be stopped.", j)
	}

}
