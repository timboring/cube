package stats

import "testing"

func TestMemoryStats(t *testing.T) {
	m, _ := GetMemoryInfo()
	t.Logf("Mem total: %v", m.MemTotal)
	t.Logf("Mem free: %v", m.MemFree)
	t.Logf("Mem available: %v", m.MemAvailable)
}

func TestCpuInfo(t *testing.T) {
	c, _ := GetCpuInfo()
	t.Logf("CPU count: %v", len(c.Processors))
}

func TestCpuStats(t *testing.T) {
	c, _ := GetCpuStats()
	t.Logf("CPU count: %v", c)
}

func TestDiskStats(t *testing.T) {
	d, _ := GetDiskInfo()
	t.Logf("Disk total: %v", d.All)
	t.Logf("Disk used: %v", d.Used)
	t.Logf("Disk free: %v", d.Free)
}

func TestLoadAvg(t *testing.T) {
	l, _ := GetLoadAvg()
	t.Logf("1 min avg: %v", l.Last1Min)
	t.Logf("5 min avg: %v", l.Last5Min)
	t.Logf("15 min avg: %v", l.Last15Min)
}
