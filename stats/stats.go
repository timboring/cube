package stats

import (
	"github.com/c9s/goprocinfo/linux"
)

type Stats struct {
	MemStats  *linux.MemInfo
	DiskStats *linux.Disk
	CpuInfo   *linux.CPUInfo
	CpuStats  *linux.CPUStat
	LoadStats *linux.LoadAvg
	TaskCount int
}

func (s *Stats) MemUsedKb() uint64 {
	return s.MemStats.MemTotal - s.MemStats.MemAvailable
}

func (s *Stats) MemUsedPercent() uint64 {
	return s.MemStats.MemAvailable / s.MemStats.MemTotal
}

func (s *Stats) MemFreeKb() uint64 {
	return s.MemStats.MemFree
}

func (s *Stats) MemAvailableKb() uint64 {
	return s.MemStats.MemAvailable
}

func (s *Stats) MemTotalKb() uint64 {
	return s.MemStats.MemTotal
}

func (s *Stats) DiskTotal() uint64 {
	return s.DiskStats.All
}

func (s *Stats) DiskFree() uint64 {
	return s.DiskStats.Free
}

func (s *Stats) DiskUsed() uint64 {
	return s.DiskStats.Used
}

func (s *Stats) CpuCores() int {
	return s.CpuInfo.NumCore()
}

func GetStats() *Stats {
	MemStats, _ := GetMemoryInfo()
	DiskStats, _ := GetDiskInfo()
	CpuInfo, _ := GetCpuInfo()
	LoadStats, _ := GetLoadAvg()
	CpuStats, _ := GetCpuStats()

	s := Stats{
		MemStats:  MemStats,
		DiskStats: DiskStats,
		CpuInfo:   CpuInfo,
		CpuStats:  CpuStats,
		LoadStats: LoadStats,
	}

	return &s
}

// GetMemoryInfo See https://godoc.org/github.com/c9s/goprocinfo/linux#MemInfo
func GetMemoryInfo() (*linux.MemInfo, error) {
	memstats, err := linux.ReadMemInfo("/proc/meminfo")
	if err != nil {
		return nil, err
	}

	return memstats, nil
}

// GetDiskInfo See https://godoc.org/github.com/c9s/goprocinfo/linux#Disk
func GetDiskInfo() (*linux.Disk, error) {
	diskstats, err := linux.ReadDisk("/")
	if err != nil {
		return nil, err
	}

	return diskstats, nil
}

// GetCpuInfo See https://godoc.org/github.com/c9s/goprocinfo/linux#CPUInfo
func GetCpuInfo() (*linux.CPUInfo, error) {
	cpuinfo, err := linux.ReadCPUInfo("/proc/cpuinfo")
	if err != nil {
		return nil, err
	}

	return cpuinfo, nil
}

// GetCpuInfo See https://godoc.org/github.com/c9s/goprocinfo/linux#CPUStat
func GetCpuStats() (*linux.CPUStat, error) {
	stats, err := linux.ReadStat("/proc/stat")
	if err != nil {
		return nil, err
	}

	return &stats.CPUStatAll, nil
}

// GetLoadAvg See https://godoc.org/github.com/c9s/goprocinfo/linux#LoadAvg
func GetLoadAvg() (*linux.LoadAvg, error) {
	loadavg, err := linux.ReadLoadAvg("/proc/loadavg")
	if err != nil {
		return nil, err
	}

	return loadavg, nil
}
