package worker

import (
	"cube/job"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"sync"

	"github.com/go-chi/chi"
	"github.com/google/uuid"
)

func (a *Api) initRouter() {
	a.router = chi.NewRouter()
	a.router.Route("/tasks", func(r chi.Router) {
		r.Post("/", a.AddTaskHandler)
		r.Get("/", a.GetTasksHandler)
		r.Route("/{jobID}", func(r chi.Router) {
			r.Delete("/", a.StopTaskHandler)
		})
	})
	a.router.Route("/stats", func(r chi.Router) {
		r.Get("/", a.GetStatsHandler)
	})
}

func (a *Api) AddTaskHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		log.Printf("Error reading request body: %v", err)
		w.WriteHeader(400)
	}

	t := job.Task{}
	t.Mu = &sync.Mutex{}
	err = json.Unmarshal(body, &t)
	if err != nil {
		log.Printf("Error unmarshalling body: %v", err)
		w.WriteHeader(400)
	}

	a.worker.addTask(t)
	log.Printf("Added task %v\n", t.ID)
	w.WriteHeader(204)
}

func (a *Api) GetStatsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(a.worker.Stats)
}

// GetTasksHandler returns all tasks
func (a *Api) GetTasksHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	queryResult := a.worker.Db.List()
	tasks := queryResult.Result.([]*job.Task)
	json.NewEncoder(w).Encode(tasks)
}

func (a *Api) StopTaskHandler(w http.ResponseWriter, r *http.Request) {
	jobID := chi.URLParam(r, "jobID")
	if jobID == "" {
		log.Printf("No jobID passed in request.")
		w.WriteHeader(400)
	}

	j, _ := uuid.Parse(jobID)
	result := a.worker.Db.Get(j.String())
	if result.Error != nil {
		w.WriteHeader(404)
	}
	task := result.Result.(*job.Task)
	task.State = job.Completed
	a.worker.addTask(*task)

	w.WriteHeader(204)
}
