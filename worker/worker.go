package worker

import (
	"cube/job"
	"cube/stats"
	"cube/store"
	"fmt"
	"log"
	"time"

	"github.com/golang-collections/collections/queue"
)

// Worker represents an instance of a worker
type Worker struct {
	Queue     queue.Queue
	Db        store.Store
	Stats     *stats.Stats
	TaskCount int
}

// New constructor
func New() *Worker {
	db := store.NewStore(store.Task, "worker_tasks.db", 0600, "tasks")
	return &Worker{Queue: *queue.New(), Db: db}
}

func (w *Worker) addTask(t job.Task) {
	w.Queue.Enqueue(t)
	w.Db.Put(t.ID.String(), &t)
}

func (w *Worker) CollectStats() {
	for {
		log.Println("Collecting stats")
		w.Stats = stats.GetStats()
		w.Stats.TaskCount = w.TaskCount
		// TODO: make sleep time here configurable
		time.Sleep(15 * time.Second)
	}
}

func (w *Worker) UpdateTaskCount() {
	for {
		log.Println("Updating task count")
		w.TaskCount = w.Db.Count()
		time.Sleep(15 * time.Second)
	}
}

// RunTask process a task event and dispatch it to the appropriate method
func (w *Worker) RunTask() job.DockerResult {
	log.Printf("[DEBUG] Length of scheduled queue: %d\n", w.Queue.Len())
	log.Println("-------------------------")
	t := w.Queue.Dequeue()
	if t == nil {
		log.Println("No tasks in the queue")
		return job.DockerResult{Error: nil}
	}
	log.Printf("[DEBUG] Length of scheduled queue: %d\n", w.Queue.Len())

	// Have to cast from interface{} to job.Task :-(
	task := t.(job.Task)
	queryResult := w.Db.Get(task.ID.String())
	taskPersisted := queryResult.Result.(*job.Task)
	if queryResult.Error != nil {
		errmsg := fmt.Errorf("Task %v not found", task.ID.String())
		return job.DockerResult{Error: errmsg}
	}

	var result job.DockerResult
	if job.Find([]job.State{taskPersisted.State}, task.State) {
		switch task.State {
		case job.Scheduled:
			result = w.StartTask(task)
		case job.Completed:
			result = w.StopTask(task)
		default:
			errmsg := fmt.Errorf("There is not a valid transition for state %v", task.State)
			result.Error = errmsg
		}
	}
	return result
}

// StartTask start a task on the selected node
func (w *Worker) StartTask(t job.Task) job.DockerResult {
	t.Mu.Lock()
	t.StartTime = time.Now().UTC()
	t.Mu.Unlock()
	d := job.Docker{Config: t}
	result := d.Run()
	if result.Error != nil {
		log.Printf("Err running task %v: %v\n", t.ID, result.Error)
		d.TransitionState(&t, job.Failed)
		w.Db.Put(t.ID.String(), &t)
		return result
	}

	d.TransitionState(&t, job.Running)

	t.Mu.Lock()
	defer t.Mu.Unlock()
	t.Runtime.ContainerID = result.ContainerId
	w.Db.Put(t.ID.String(), &t)

	log.Printf("[DEBUG] Length of scheduled queue: %d\n", w.Queue.Len())

	return result
}

// StopTask stop a running task
func (w *Worker) StopTask(t job.Task) job.DockerResult {
	t.FinishTime = time.Now().UTC()
	d := job.Docker{Config: t}
	result := d.Stop()
	if result.Error != nil {
		log.Printf("Error stopping container %v: %v", t.Runtime.ContainerID, result.Error)
	}
	d.TransitionState(&t, job.Completed)
	w.Db.Put(t.ID.String(), &t)
	log.Printf("Stopped and removed container %v for task %v", t.Runtime.ContainerID, t.ID)

	return result
}

func (w *Worker) String() string {
	return fmt.Sprintf("Worker task queue - %d", w.Queue.Len())
}
