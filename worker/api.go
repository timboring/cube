package worker

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
)

type ApiConfig struct {
	Address string
	Port    int
}

type Api struct {
	cfg     *ApiConfig
	stopped bool
	worker  *Worker
	router  *chi.Mux
}

func NewApi(cfg *ApiConfig, w *Worker) *Api {
	return &Api{
		cfg:    cfg,
		worker: w,
	}
}

func (a *Api) Start() {
	a.initRouter()
	http.ListenAndServe(fmt.Sprintf("%s:%d", a.cfg.Address, a.cfg.Port), a.router)
}

func (a *Api) Stop() error {
	return nil
}
