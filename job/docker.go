package job

import (
	"context"
	"io"
	"log"
	"os"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
	"github.com/moby/moby/pkg/stdcopy"
)

type DockerResult struct {
	Error       error
	Action      string
	ContainerId string
	Result      string
}

type DockerRuntime struct {
	ContainerId string
}

type Resources struct {
	// cpu limit
	// TODO: this doesn't appear to be supported by the api, cli is --cpus
	cpu int
	// memory in bytes
	Memory int64
}

// Config used to start a container
type Config struct {
	// Hostname
	Hostname string
	// Name of the task
	Name string
	// ContainerName of the container
	ContainerName string
	// AttachStdin boolean which determines if stdin should be attached
	AttachStdin bool
	// AttachStdout boolean which determines if stdout should be attached
	AttachStdout bool
	// AttachStderr boolean which determines if stderr should be attached
	AttachStderr bool
	// ExposedPorts list of ports exposed
	ExposedPorts nat.PortSet
	// Cmd to be run inside container (optional)
	Cmd []string
	// Image used to run the container
	Image string
	// Resources to specify for the container
	Resources Resources
	// Env variables
	Env []string
	// RestartPolicy for the container ["", "always", "unless-stopped", "on-failure"]
	RestartPolicy string
}

// Docker representation of a task that runs a Docker container
type Docker struct {
	Config  Task
	Runtime DockerRuntime
}

// Run task as a Docker container
func (d *Docker) Run() DockerResult {
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		log.Printf("Error creating Docker client: %v\n", err)
		return DockerResult{Error: err}
	}

	reader, err := cli.ImagePull(ctx, d.Config.Image, types.ImagePullOptions{})
	if err != nil {
		log.Printf("Error pulling image %s: %v\n", d.Config.Image, err)
		return DockerResult{Error: err}
	}
	io.Copy(os.Stdout, reader)

	rp := container.RestartPolicy{
		Name: d.Config.RestartPolicy,
	}

	pm := make(nat.PortMap)
	for k, v := range d.Config.PortBindings {
		proto, port := nat.SplitProtoPort(k)
		pb := nat.PortBinding{HostIP: "", HostPort: string(v)}
		key, _ := nat.NewPort(proto, port)
		pm[key] = []nat.PortBinding{pb}

	}

	r := container.Resources{
		Memory: d.Config.Memory,
	}

	hc := container.HostConfig{
		RestartPolicy: rp,
		PortBindings:  pm,
		Resources:     r,
	}

	resp, err := cli.ContainerCreate(ctx, &container.Config{
		Image:        d.Config.Image,
		Tty:          false,
		Env:          d.Config.Env,
		ExposedPorts: d.Config.ExposedPorts,
	}, &hc, nil, nil, d.Config.Name)
	if err != nil {
		log.Printf("Error creating container using image %s: %v\n", d.Config.Image, err)
		return DockerResult{Error: err}
	}

	if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		log.Printf("Error starting container %s: %v\n", resp.ID, err)
		return DockerResult{Error: err}
	}

	d.Config.Runtime.ContainerID = resp.ID

	out, err := cli.ContainerLogs(ctx, resp.ID, types.ContainerLogsOptions{ShowStdout: true, ShowStderr: true})
	if err != nil {
		log.Printf("Error getting logs for container %s: %v\n", resp.ID, err)
		return DockerResult{Error: err}
	}

	stdcopy.StdCopy(os.Stdout, os.Stderr, out)

	return DockerResult{ContainerId: resp.ID, Action: "start", Result: "success"}
}

// Stop a task
func (d *Docker) Stop() DockerResult {
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		panic(err)
	}

	log.Printf("Attempting to stop container %v", d.Config.Runtime.ContainerID)
	err = cli.ContainerStop(ctx, d.Config.Runtime.ContainerID, nil)
	if err != nil {
		panic(err)
	}

	err = cli.ContainerRemove(ctx, d.Config.Runtime.ContainerID, types.ContainerRemoveOptions{RemoveVolumes: true, RemoveLinks: false, Force: false})
	if err != nil {
		panic(err)
	}

	return DockerResult{Action: "stop", Result: "success", Error: nil}
}

// Validate a task
func (d *Docker) Validate() bool {
	return true
}

// TransitionState helper function to transition state
func (d *Docker) TransitionState(t *Task, desired State) {
	if Find(TransitionMap[t.State], desired) == true {
		t.Mu.Lock()
		defer t.Mu.Unlock()
		t.State = desired
	} else {
		log.Printf("State %v is not a valid transition for %v", desired, t.State)
	}
}

func newDocker(config *Task) *Docker {
	return &Docker{
		Config: *config,
	}

}
