package job

import (
	"fmt"
	"log"
	"os"
	"os/exec"

	"github.com/google/uuid"
)

type CommandResult struct {
	Error error
}

// Command representation of a task that runs as a Unix command
type Command struct {
	// Id as UUID
	ID uuid.UUID
	// name is the name of the task
	Name string
	// Command to run
	Command string
	// Args to pass to command
	Args []string
	// State of task
	State  State
	stdin  string
	stdout string
	stderr string
}

func newCommand() *Command {
	return &Command{}
}
func (c *Command) String() string {
	return fmt.Sprintf("Task %s <%s> (Command: %v, Args: %v, State: %v)", c.Name, c.ID, c.Command, c.Args, c.State)
}

// Run a task
func (c *Command) Run() CommandResult {
	cmd := exec.Command(c.Command, c.Args...)
	log.Printf("Running task %v\n", c)
	err := cmd.Run()
	result := CommandResult{Error: err}
	return result
}

// Validate a task
func (c *Command) Validate() bool {
	_, err := os.Stat(c.Command)
	if err != nil {
		log.Fatalf("Command %s does not exist", c.Command)
		return false
	}
	return true
}

// TODO: can this method be move to task.go?

// TransitionState helper function to transition states
func (c *Command) TransitionState(desired State) {
	// if desired state is a value in state slice for current state,
	// transition to the desired state
	if Find(TransitionMap[c.State], desired) == true {
		c.State = desired
	} else {
		log.Printf("State %v is not a valid transition for %v", desired, c.State)
	}
}
