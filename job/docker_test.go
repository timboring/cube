package job

import (
	"testing"

	"github.com/google/uuid"
)

func TestDockerRunAndStop(t *testing.T) {
	task := NewTask("rabbitmq", "rabbitmq:3-management", 1024, 4096/1000/1000)

	u, _ := uuid.NewRandom()
	task.ID = u
	d := newDocker(task)

	result := d.Run()
	if result.Result != "success" {
		t.Errorf("Running task was not successful")
	}

	// Cleanup
	r := d.Stop()
	if r.Result != "success" {
		t.Errorf("Stopping task was not successful")
	}
}

func TestDockerTransitionState(t *testing.T) {
	task := NewTask("rabbitmq", "rabbitmq:3-management", 1024, 4096/1000/1000)

	var tests = []struct {
		startState State
		endState   State
	}{
		{Pending, Scheduled},
		{Scheduled, Running},
	}

	d := newDocker(task)
	for _, test := range tests {
		task.State = test.startState
		d.TransitionState(task, test.endState)

		if task.State != test.endState {
			t.Errorf("TestCommandTransitionState(%q) = %v", test.startState, task.State)
		}
	}
}
