package job

// Job representation of a Job
type Job struct {
	Priority int
	Tasks    []Docker
	Name     string
}
