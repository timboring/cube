package job

import (
	"fmt"
	"testing"

	"github.com/google/uuid"
)

func TestCommandConstruction(t *testing.T) {
	cmd := newCommand()

	if cmd.Name != "" {
		t.Error("Task name is not nil")
	}

}

func TestCommandWithFields(t *testing.T) {
	u, _ := uuid.NewRandom()
	cmd := newCommand()
	cmd.ID = u
	cmd.Name = "FooTask"
	cmd.Command = "echo"
	cmd.Args = []string{"hello", "world"}
	cmd.State = Pending

	want := fmt.Sprintf("Task FooTask <%v> (Command: echo, Args: [hello world], State: 0)", u)

	if got := cmd.String(); got != want {
		t.Errorf("Wanted: %v, Got: %v", want, got)
	}
}

func TestCommandTransitionState(t *testing.T) {
	var tests = []struct {
		startState State
		endState   State
	}{
		{Pending, Scheduled},
		{Scheduled, Running},
	}

	cmd := newCommand()
	for _, test := range tests {
		cmd.State = test.startState
		cmd.TransitionState(test.endState)

		if cmd.State != test.endState {
			t.Errorf("TestCommandTransitionState(%q) = %v", test.startState, cmd.State)
		}
	}
}
