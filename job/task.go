package job

import (
	"encoding/json"
	"fmt"
	"sync"
	"time"

	"github.com/docker/docker/api/types/container"
	"github.com/docker/go-connections/nat"
	"github.com/google/uuid"
)

// State is the state a task is in at any given time
type State int

const (
	// Pending initial state of task upon being submitted to manager
	Pending State = iota
	// Scheduled state of task upon being submitted to worker by the manager
	Scheduled
	// Completed state of task upon completion
	Completed
	// Running state of task during execution
	Running
	// Failed state of task when it has failed for any reason
	Failed
)

func (s State) String() []string {
	return []string{"Pending", "Scheduled", "Completed", "Running", "Failed"}
}

// Action an action performed by a task event
type Action int

const (
	// Schedule an event to schedule a task
	Schedule Action = iota
	// Start an event to start a task
	Start
	// Stop an event to stop a task
	Stop
	// Fail an event to mark a task as failed
	Fail
	// Restart an event to restart a stopped or failed task
	Restart
)

// Priority of a task
type Priority int

const (
	// System level tasks
	System     Priority = 100
	// Production level tasks
	Production Priority = 200
	// Batch level tasks
	Batch      Priority = 300
	// BestEffort tasks
	BestEffort Priority = 400
)

func (a Action) String() []string {
	return []string{"Scheduled", "Start", "Stop", "Fail", "Restart"}
}

// Runtime is used to hold runtime data about a task
type Runtime struct {
	// ContainerID assigned by Docker
	ContainerID string
	// Node where the container is running
	Node string
}

// Task details about individual task
type Task struct {
	Mu            *sync.Mutex
	ID            uuid.UUID
	Name          string
	Image         string
	Env           []string
	Memory        int64
	Disk          int64
	ExposedPorts  nat.PortSet
	PortBindings  map[string]string
	HostConfig    *container.HostConfig
	RestartPolicy string
	State         State
	Runtime       Runtime
	StartTime     time.Time
	FinishTime    time.Time
}

// NewTask is a factory that returns an initialized task
func NewTask(name string, image string, disk int64, memory int64) *Task {
	return &Task{
		Mu:     &sync.Mutex{},
		Name:   name,
		Image:  image,
		Memory: memory,
		Disk:   disk,
	}
}

func (t Task) String() string {
	s, err := json.Marshal(t)
	if err != nil {
		fmt.Printf("Error marshalling task: %v", err)
	}
	return string(s)
}

// TaskEvent represents an individual task event
type TaskEvent struct {
	ID        uuid.UUID
	TaskID    uuid.UUID
	Action    Action
	Timestamp time.Time
	Task      Task
}

// TransitionMap mapping of state transitions
var TransitionMap = map[State][]State{
	Pending:   []State{Scheduled},
	Scheduled: []State{Running, Failed},
	Running:   []State{Completed, Failed},
	Completed: []State{},
	Failed:    []State{},
}

// Find is a helper function to evaluation whether a task can transition from one state to another
func Find(source []State, val State) bool {
	for _, item := range source {
		if item == val {
			return true
		}
	}
	return false
}
