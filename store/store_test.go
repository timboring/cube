package store

import (
	"cube/job"
	"fmt"
	"os"
	"testing"

	"github.com/google/uuid"
)

func TestTaskStoreCreate(t *testing.T) {
	ts := NewStore(Task, "tasks.db", 0600, "tasks")
	t.Logf("Create task db: %v", ts)
	ts.Close()
	os.Remove("tasks.db")
}

func TestTaskStorePut(t *testing.T) {
	ts := NewStore(Task, "tasks.db", 0600, "tasks")
	defer ts.Close()
	uuid, _ := uuid.NewRandom()
	task := job.Task{
		ID: uuid,
	}
	ts.Put(task.ID.String(), &task)

	result := ts.Get(task.ID.String())
	if result.Error != nil {
		t.Errorf("Expected err to be nil. Got %v", result.Error)
	}

	persistedTask := result.Result.(*job.Task)
	if persistedTask.ID != task.ID {
		t.Errorf("Expected task with ID %v. Got %v", task.ID.String(), persistedTask.ID.String())
	}
	os.Remove("tasks.db")
}

func TestTaskStoreCount(t *testing.T) {
	ts := NewStore(Task, "tasks.db", 0600, "tasks")
	defer ts.Close()
	for i := 0; i < 5; i++ {
		uuid, _ := uuid.NewRandom()
		task := job.Task{ID: uuid}
		ts.Put(task.ID.String(), &task)
	}
	count := ts.Count()
	if count != 5 {
		t.Errorf("Expected 5 tasks; received %v", count)
	}
}

func TestTaskStoreRetrieveNonExistentTask(t *testing.T) {
	ts := NewStore(Task, "tasks.db", 0600, "tasks")
	defer ts.Close()

	id, _ := uuid.NewRandom()
	task := job.Task{
		ID: id,
	}
	ts.Put(task.ID.String(), &task)

	u, _ := uuid.NewRandom()
	result := ts.Get(u.String())
	if result.Error == nil {
		t.Errorf("Expected nil\n")
	}
	os.Remove("tasks.db")
}

func TestTaskStoreList(t *testing.T) {
	ts := NewStore(Task, "tasks.db", 0600, "tasks")
	defer ts.Close()

	for i := 0; i < 3; i++ {
		id, _ := uuid.NewRandom()
		task := job.Task{ID: id}
		ts.Put(task.ID.String(), &task)
	}

	result := ts.List()
	tasks := result.Result.([]*job.Task)
	if len(tasks) != 3 {
		t.Errorf("Expected 3 tasks. Got %v", len(tasks))
	}
	os.Remove("tasks.db")
}

func TestTaskStoreListWhenEmpty(t *testing.T) {
	ts := NewStore(Task, "tasks.db", 0600, "tasks")
	defer ts.Close()

	result := ts.List()
	tasks := result.Result.([]*job.Task)
	if len(tasks) != 0 {
		t.Errorf("Expected 0 tasks. Got %v", len(tasks))
	}
	os.Remove("tasks.db")
}

func TestEventStoreCreate(t *testing.T) {
	es := NewStore(Event, "events.db", 0600, "events")
	t.Logf("Create task db: %v", es)
	es.Close()
	os.Remove("events.db")
}

func TestEventStoreCount(t *testing.T) {
	es := NewStore(Event, "tasks.db", 0600, "events")
	defer es.Close()
	for i := 0; i < 5; i++ {
		uuid, _ := uuid.NewRandom()
		event := job.TaskEvent{ID: uuid}
		es.Put(event.ID.String(), &event)
	}
	count := es.Count()
	if count != 5 {
		t.Errorf("Expected 5 events; received %v", count)
	}
}

func TestEventStorePut(t *testing.T) {
	es := NewStore(Event, "events.db", 0600, "events")
	defer es.Close()
	uuid, _ := uuid.NewRandom()
	event := job.TaskEvent{
		ID: uuid,
	}
	es.Put(event.ID.String(), &event)

	result := es.Get(event.ID.String())
	persistedEvent := result.Result.(*job.TaskEvent)
	if result.Error != nil {
		t.Errorf("Expected err to be nil. Got %v", result.Error)
	}
	if event.ID != persistedEvent.ID {
		t.Errorf("Expected task with ID %v. Got %v", event.ID.String(), persistedEvent.ID.String())
	}
	os.Remove("events.db")
}

func TestEventStoreRetrieveNonExistentTask(t *testing.T) {
	es := NewStore(Event, "events.db", 0600, "events")
	defer es.Close()

	id, _ := uuid.NewRandom()
	event := job.TaskEvent{
		ID: id,
	}
	es.Put(event.ID.String(), &event)

	u, _ := uuid.NewRandom()
	result := es.Get(u.String())
	if result.Error == nil {
		t.Error("Expected error message. Got nil\n")
	}

	expected := fmt.Sprintf("event %v not found", u.String())
	if result.Error.Error() != expected {
		t.Errorf("Expected: %v. Got: %v", expected, result.Error.Error())
	}

	os.Remove("events.db")
}

func TestEventStoreList(t *testing.T) {
	es := NewStore(Event, "events.db", 0600, "events")
	defer es.Close()

	for i := 0; i < 3; i++ {
		id, _ := uuid.NewRandom()
		event := job.TaskEvent{ID: id}
		es.Put(event.ID.String(), &event)
	}

	result := es.List()
	events := result.Result.([]*job.TaskEvent)
	if len(events) != 3 {
		t.Errorf("Expected 3 events. Got %v", len(events))
	}
	os.Remove("events.db")
}

func TestEventStoreListWhenEmpty(t *testing.T) {
	es := NewStore(Event, "events.db", 0600, "events")
	defer es.Close()

	result := es.List()
	events := result.Result.([]*job.TaskEvent)
	if len(events) != 0 {
		t.Errorf("Expected 0 events. Got %v", len(events))
	}
	os.Remove("events.db")
}
