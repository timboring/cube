package store

import (
	"cube/job"
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/boltdb/bolt"
)

type Type int

const (
	Task Type = iota
	Event
)

type QueryResult struct {
	Error  error
	Result interface{}
}

type Store interface {
	Close()
	Count() int
	CreateBucket()
	Put(string, interface{})
	Get(string) QueryResult
	List() QueryResult
}

func NewStore(t Type, file string, mode os.FileMode, bucket string) Store {
	var s Store
	switch t {
	case Task:
		s = newTaskStore(file, mode, bucket)
	case Event:
		s = newEventStore(file, mode, bucket)
	}

	return s
}

type TaskStore struct {
	dbFile   string
	fileMode os.FileMode
	db       *bolt.DB
	bucket   string
}

func newTaskStore(file string, mode os.FileMode, bucket string) *TaskStore {
	db, err := bolt.Open(file, mode, nil)
	if err != nil {
		log.Fatalf("Unable to open %v\n", file)
	}
	t := TaskStore{
		dbFile:   file,
		fileMode: mode,
		db:       db,
		bucket:   bucket,
	}

	t.CreateBucket()
	return &t
}

func (t *TaskStore) Close() {
	t.db.Close()
}

func (t *TaskStore) Count() int {
	taskCount := 0
	t.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("tasks"))
		c := b.Cursor()

		for k, _ := c.First(); k != nil; k, _ = c.Next() {
			taskCount++
		}
		return nil
	})
	return taskCount
}

func (t *TaskStore) CreateBucket() {
	t.db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucket([]byte(t.bucket))
		if err != nil {
			return fmt.Errorf("create bucket %s: %s", t.bucket, err)
		}
		return nil
	})
}

func (t *TaskStore) Put(key string, task interface{}) {
	t.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(t.bucket))

		buf, err := json.Marshal(task.(*job.Task))
		if err != nil {
			return err
		}

		err = b.Put([]byte(key), buf)
		if err != nil {
			log.Printf("unable to save item %s", key)
			return err
		}
		return nil
	})
}

func (t *TaskStore) Get(key string) QueryResult {
	var task job.Task
	err := t.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(t.bucket))
		t := b.Get([]byte(key))
		if t == nil {
			return fmt.Errorf("task %v not found", key)
		}
		json.Unmarshal(t, &task)
		return nil
	})
	if err != nil {
		return QueryResult{Error: err, Result: nil}
	}
	return QueryResult{Error: nil, Result: &task}
}

func (t *TaskStore) List() QueryResult {
	var tasks []*job.Task
	t.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(t.bucket))
		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			var task job.Task
			json.Unmarshal(v, &task)
			tasks = append(tasks, &task)
		}
		return nil
	})
	return QueryResult{Result: tasks, Error: nil}
}

type EventStore struct {
	dbFile   string
	fileMode os.FileMode
	db       *bolt.DB
	bucket   string
}

func newEventStore(file string, mode os.FileMode, bucket string) *EventStore {
	db, err := bolt.Open(file, mode, nil)
	if err != nil {
		log.Fatalf("Unable to open %v\n", file)
	}
	e := EventStore{
		dbFile:   file,
		fileMode: mode,
		db:       db,
		bucket:   bucket,
	}

	e.CreateBucket()
	return &e
}

func (e *EventStore) Close() {
	e.db.Close()
}

func (e *EventStore) CreateBucket() {
	e.db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucket([]byte(e.bucket))
		if err != nil {
			return fmt.Errorf("create bucket %s: %s", e.bucket, err)
		}
		return nil
	})
}
func (e *EventStore) Count() int {
	eventCount := 0
	e.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(e.bucket))
		c := b.Cursor()

		for k, _ := c.First(); k != nil; k, _ = c.Next() {
			eventCount++
		}
		return nil
	})
	return eventCount
}

func (e *EventStore) Put(key string, task interface{}) {
	e.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(e.bucket))

		buf, err := json.Marshal(task.(*job.TaskEvent))
		if err != nil {
			return err
		}

		err = b.Put([]byte(key), buf)
		if err != nil {
			log.Printf("unable to save item %s", key)
			return err
		}
		return nil
	})
}

func (e *EventStore) Get(key string) QueryResult {
	var event job.TaskEvent
	err := e.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(e.bucket))
		t := b.Get([]byte(key))
		if t == nil {
			return fmt.Errorf("event %v not found", key)
		}
		json.Unmarshal(t, &event)
		return nil
	})

	if err != nil {
		return QueryResult{Error: err, Result: nil}
	}
	return QueryResult{Result: &event, Error: nil}
}

func (e *EventStore) List() QueryResult {
	var events []*job.TaskEvent
	e.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(e.bucket))
		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			var event job.TaskEvent
			json.Unmarshal(v, &event)
			events = append(events, &event)
		}
		return nil
	})
	return QueryResult{Error: nil, Result: events}
}
