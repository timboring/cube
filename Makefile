BUILD_DIR=./builds

.PHONY : build test clean
build:
	GOARCH=arm go build -o builds/cube-${VERSION}-armv7l ./main.go
	go build -o builds/cube-${VERSION}-amd64 ./main.go

test:
	go test -v ./...

clean:
	go clean
